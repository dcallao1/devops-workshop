# Terra-lint container build
FROM python:slim

# Install base packages
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install curl wget bash unzip git -y

# Install latest Terraform
# If jobs *ONLY* use Terraform then suggest using: 
# image:hashicorp/terraform:light

WORKDIR /tmp
RUN export latest_version=`curl https://checkpoint-api.hashicorp.com/v1/check/terraform | python -mjson.tool | grep current_version | awk '{print $2}' | sed s/\"\//g | sed s/,//g` && \
    export version=$latest_version && \
    echo $version && \
    wget "https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip" && \
    unzip *.zip && \
    mv terraform /usr/bin/terraform

# Install Terraform linter

RUN wget -q https://github.com/terraform-linters/tflint/releases/download/v0.16.2/tflint_linux_amd64.zip -O tflint.zip && unzip tflint.zip && mv tflint /bin
#
